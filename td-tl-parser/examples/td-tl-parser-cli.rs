use structopt::StructOpt;

#[derive(StructOpt)]
pub struct Args {
	pub file: std::path::PathBuf,
}

fn real_main() -> Result<(), String> {
	let args = Args::from_args();

	let src =
		std::fs::read_to_string(&args.file).map_err(|e| format!("Could not open file: {}", e))?;
	let stdout = std::io::stdout();
	let stdout = stdout.lock();
	td_tl_parser::generate::generate_and_write(&src, stdout).map_err(|e| e.to_string())?;

	Ok(())
}

fn main() {
	if let Err(e) = real_main() {
		eprintln!("{}", e);
		std::process::exit(1);
	}
}
