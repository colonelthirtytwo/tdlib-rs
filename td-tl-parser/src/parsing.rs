use indexmap::IndexMap;
use pest::iterators::{
	Pair,
	Pairs,
};
use regex::Regex;
use std::collections::HashSet;

#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct TLParser;

lazy_static::lazy_static! {
	static ref RE_DOC: Regex = Regex::new(r"@([a-zA-Z0-9_]+) ([^@]*)").unwrap();
}

#[derive(Clone, Debug)]
pub struct Decl {
	pub name: String,
	pub super_decl: String,
	pub section: Option<String>,
	pub fields: IndexMap<String, Field>,
	pub documentation: String,
}
impl Decl {
	pub fn parse(pair: Pair<Rule>, section: Option<String>) -> Self {
		assert_eq!(pair.as_rule(), Rule::decl);
		let mut pair = pair.into_inner();

		let doc_comment = if pair.peek().unwrap().as_rule() == Rule::decl_doc_comment {
			remove_comment_prefix(pair.next().unwrap().as_str())
		} else {
			String::new()
		};

		let name = String::from(pair.next().unwrap().as_str());

		let mut fields = IndexMap::new();
		while Some(Rule::field) == pair.peek().map(|v| v.as_rule()) {
			let field = Field::parse(pair.next().unwrap());
			fields.insert(field.name.clone(), field);
		}

		let super_decl = String::from(pair.next().unwrap().as_str());

		assert!(pair.next().is_none());

		let mut documentation = String::new();
		for m in RE_DOC.captures_iter(&doc_comment) {
			let fname = m[1].trim();
			let fdoc = m[2].trim();

			if fname == "description" {
				documentation = String::from(fdoc);
			} else {
				let fname = if fname == "param_description" {
					"description"
				} else {
					fname
				};
				fields
					.get_mut(fname)
					.expect(&format!("Doc for nonexistent field {} in {}", name, fname))
					.documentation = String::from(fdoc);
			}
		}

		Self {
			name,
			super_decl,
			section,
			fields,
			documentation,
		}
	}

	pub fn is_empty(&self) -> bool {
		self.fields.is_empty()
	}

	pub fn is_primitive(&self) -> bool {
		is_primitive(&self.name)
	}

	pub fn is_function(&self) -> bool {
		self.section.as_ref().map(String::as_str) == Some("functions")
	}

	pub fn has_class(&self) -> bool {
		!self.is_function() && self.rust_name() != self.super_decl
	}

	pub fn rust_name(&self) -> String {
		uppercase_first_letter(&self.name)
	}

	pub fn variant_name(&self, class_name: &str) -> String {
		let rust_name = self.rust_name();
		if rust_name.starts_with(class_name)
			&& rust_name[class_name.len()..]
				.chars()
				.next()
				.unwrap()
				.is_uppercase()
		{
			String::from(&self.rust_name()[class_name.len()..])
		} else {
			rust_name
		}
	}
}

#[derive(Clone, Debug)]
pub struct Field {
	pub name: String,
	pub typ: FieldType,

	pub documentation: String,
}
impl Field {
	pub fn parse(pair: Pair<Rule>) -> Self {
		assert_eq!(pair.as_rule(), Rule::field);
		let mut pair = pair.into_inner();
		let name = String::from(pair.next().unwrap().as_str());
		let typ = FieldType::parse(pair.next().unwrap());
		assert!(pair.next().is_none());
		Self {
			name,
			typ,
			documentation: String::new(),
		}
	}
}

#[derive(Clone, Debug)]
pub struct FieldType {
	pub name: String,
	pub generic: Option<Box<FieldType>>,
}
impl FieldType {
	pub fn parse(pair: Pair<Rule>) -> Self {
		assert_eq!(pair.as_rule(), Rule::typ);
		let mut pair = pair.into_inner();
		let name = String::from(pair.next().unwrap().as_str());
		let generic = pair.next().map(Self::parse).map(Box::new);
		assert!(pair.next().is_none());
		assert!(generic.is_none() || name == "vector");
		Self { name, generic }
	}

	pub fn rust_type(&self) -> String {
		match self.name.as_str() {
			"Bool" => "bool".into(),
			"int32" => "i32".into(),
			"int53" | "int64" => "i64".into(),
			"double" => "f64".into(),
			"string" => "String".into(),
			"bytes" => "Vec<u8>".into(),
			"vector" => format!("Vec<{}>", self.generic.as_ref().unwrap().rust_type()),
			_ => uppercase_first_letter(&self.name),
		}
	}

	pub fn most_inner_type(&self) -> &str {
		if let Some(generic) = self.generic.as_ref() {
			generic.most_inner_type()
		} else {
			&self.name
		}
	}
}

#[derive(Clone, Debug)]
pub struct Class {
	pub name: String,
	pub documentation: String,
}
impl Class {
	pub fn parse(s: &str) -> (String, String) {
		let mut name = String::new();
		let mut doc = String::new();
		for m in RE_DOC.captures_iter(s) {
			let fname = &m[1];
			let fdoc = &m[2];

			if fname == "class" {
				name = String::from(fdoc.trim());
			} else {
				doc = String::from(fdoc.trim());
			}
		}
		(name, doc)
	}
}

#[derive(Clone, Debug)]
pub struct API {
	pub decls: IndexMap<String, Decl>,
	pub classes: IndexMap<String, Class>,
}
impl API {
	pub fn parse(iter: Pairs<Rule>) -> Self {
		let mut current_section = None;
		let mut decls = IndexMap::new();
		let mut classes = IndexMap::new();

		for pair in iter {
			match pair.as_rule() {
				Rule::decl => {
					let decl = Decl::parse(pair, current_section.clone());
					if decl.has_class() && !classes.contains_key(&decl.super_decl) {
						let cls = Class {
							name: decl.super_decl.clone(),
							documentation: String::new(),
						};
						classes.insert(cls.name.clone(), cls);
					}
					decls.insert(decl.name.clone(), decl);
				}
				Rule::vec_decl => {}
				Rule::section => {
					let section_name = String::from(pair.into_inner().next().unwrap().as_str());
					current_section = Some(section_name);
				}
				Rule::class_doc_comment => {
					let text = remove_comment_prefix(pair.as_str());
					let (name, documentation) = Class::parse(&text);

					classes
						.entry(name.clone())
						.and_modify(|c| c.documentation = documentation.clone())
						.or_insert_with(|| Class {
							name,
							documentation,
						});
				}
				Rule::EOI => {}
				_ => {
					panic!("Not expecting rule {:?} here", pair.as_rule());
				}
			}
		}

		Self { decls, classes }
	}

	pub fn class_implementors<'a>(&'a self, cls_name: &'a str) -> impl Iterator<Item = &'a Decl> {
		self.decls
			.values()
			.filter(|d| !d.is_function())
			.filter(move |d| d.super_decl == cls_name)
	}

	pub fn type_contains_type(&self, haystack: &str, needle: &str) -> bool {
		self.type_contains_type_rec(haystack, needle, &mut HashSet::new())
	}

	fn type_contains_type_rec<'a>(
		&'a self,
		haystack: &'a str,
		needle: &str,
		seen: &mut HashSet<&'a str>,
	) -> bool {
		if haystack == needle {
			return true;
		}
		if seen.contains(haystack) {
			return false;
		}
		seen.insert(haystack);
		if let Some(cls) = self.classes.get(haystack) {
			return self
				.class_implementors(&cls.name)
				.any(|decl| self.type_contains_type_rec(&decl.name, needle, seen));
		}
		if let Some(decl) = self.decls.get(haystack) {
			return !decl.is_primitive()
				&& decl.fields.values().any(|field| {
					self.type_contains_type_rec(field.typ.most_inner_type(), needle, seen)
				});
		}
		unreachable!();
	}
}

fn remove_comment_prefix(s: &str) -> String {
	s.split("\n")
		.map(|mut s| {
			if s.starts_with("//") {
				s = &s[2..];
			}
			if s.ends_with("\r") {
				s = &s[..(s.len() - 2)];
			}
			s
		})
		.collect()
}

fn uppercase_first_letter(s: &str) -> String {
	format!("{}{}", s.chars().next().unwrap().to_uppercase(), &s[1..])
}

fn is_primitive(name: &str) -> bool {
	[
		"double",
		"string",
		"int32",
		"int53",
		"int64",
		"bytes",
		"Bool",
		"boolTrue",
		"boolFalse",
	]
	.iter()
	.find(|&&s| s == name)
	.is_some()
}
