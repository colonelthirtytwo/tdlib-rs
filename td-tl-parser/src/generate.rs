use pest::Parser;
use std::io;

use crate::parsing::*;

pub fn generate_and_write<W: io::Write>(
	tl: &str,
	mut out: W,
) -> Result<(), Box<dyn std::error::Error>> {
	let pairs = TLParser::parse(Rule::main, tl)?;
	let api = API::parse(pairs);

	writeln!(out, "use serde::{{Serialize, Deserialize}};")?;
	writeln!(out, "use crate::traits::*;")?;
	writeln!(out, "use crate::serde_util::*;")?;

	writeln!(out, "pub mod objects {{\n\tuse super::{{*, classes::*}};")?;
	for decl in api.decls.values() {
		if decl.is_primitive() || decl.is_function() {
			continue;
		}
		emit_decl(&mut out, decl, &api)?;
	}
	writeln!(out, "}}")?;

	writeln!(out, "pub mod classes {{\n\tuse super::{{*, objects::*}};")?;
	for cls in api.classes.values() {
		emit_class(&mut out, cls, &api)?;
	}
	writeln!(out, "}}")?;

	writeln!(
		out,
		"pub mod functions {{\n\tuse super::{{*, objects::*, classes::*}};"
	)?;
	for decl in api.decls.values() {
		if decl.is_primitive() || !decl.is_function() {
			continue;
		}
		emit_decl(&mut out, decl, &api)?;
	}
	writeln!(out, "}}")?;
	Ok(())
}

fn emit_decl<W: io::Write>(
	mut out: W,
	decl: &Decl,
	api: &API,
) -> Result<(), Box<dyn std::error::Error>> {
	writeln!(
		out,
		"\t#[derive(Clone,Debug,Serialize,Deserialize{})]",
		additional_derives(&decl.name)
	)?;
	writeln!(out, "\t#[doc = {:?}]", decl.documentation)?;

	let rust_name = decl.rust_name();
	writeln!(out, "\tpub struct {} {{", rust_name)?;
	for field in decl.fields.values() {
		let name = if field.name == "type" {
			writeln!(out, "\t\t#[serde(rename=\"type\")]")?;
			"typ"
		} else {
			&field.name
		};

		if field.typ.name == "bytes" {
			writeln!(out, "\t\t#[serde(serialize_with=\"ser_base64\")]")?;
			writeln!(out, "\t\t#[serde(deserialize_with=\"de_base64\")]")?;
		} else if field.typ.name == "int64" {
			writeln!(out, "\t\t#[serde(serialize_with=\"ser_large_int\")]")?;
			writeln!(out, "\t\t#[serde(deserialize_with=\"de_large_int\")]")?;
		}

		let mut typ = field.typ.rust_type();
		if api.type_contains_type(field.typ.most_inner_type(), &decl.name) {
			typ = format!("Box<{}>", typ);
		}
		if can_be_null(&decl.name, field) {
			typ = format!("Option<{}>", typ);
		}

		writeln!(out, "\t\t#[doc = {:?}]", field.documentation)?;
		writeln!(out, "\t\tpub {}: {},", name, typ)?;
	}
	writeln!(out, "\t}}")?;
	if decl.is_function() {
		writeln!(out, "\timpl TdFunction for {} {{", rust_name)?;
		writeln!(out, "\t\ttype Response = {};", decl.super_decl)?;
		writeln!(
			out,
			"\t\tfn type_name(&self) -> &'static str {{ {:?} }}",
			decl.name
		)?;
		writeln!(out, "\t}}")?;
	} else {
		writeln!(out, "\timpl TdType for {} {{", rust_name)?;
		writeln!(
			out,
			"\t\tfn type_name(&self) -> &'static str {{ {:?} }}",
			decl.name
		)?;
		writeln!(out, "\t}}")?;

		if decl.has_class() {
			writeln!(out, "\timpl TdClassImpl for {} {{", rust_name)?;
			writeln!(out, "\t\ttype Class = {};", decl.super_decl)?;
			writeln!(
				out,
				"\t\tfn upcast(self) -> Self::Class {{ {}::{}(self) }}",
				decl.super_decl,
				decl.variant_name(&decl.super_decl)
			)?;
			writeln!(out, "\t\tfn downcast(v: Self::Class) -> Result<Self, Self::Class> {{ match v {{ {}::{}(v) => Result::Ok(v), v @ _ => Result::Err(v) }} }}",
				decl.super_decl, decl.variant_name(&decl.super_decl))?;
			writeln!(out, "\t}}")?;
		}
	}

	writeln!(out)?;
	Ok(())
}

fn emit_class<W: io::Write>(
	mut out: W,
	cls: &Class,
	api: &API,
) -> Result<(), Box<dyn std::error::Error>> {
	if cls.name == "Bool" {
		return Ok(());
	}
	writeln!(
		out,
		"\t#[derive(Clone,Debug,Serialize,Deserialize{})]",
		additional_derives(&cls.name)
	)?;
	writeln!(out, "\t#[serde(tag=\"@type\")]")?;
	writeln!(out, "\t#[doc = {:?}]", cls.documentation)?;
	writeln!(out, "\tpub enum {} {{", cls.name)?;
	for decl in api.class_implementors(&cls.name) {
		writeln!(out, "\t\t#[serde(rename={:?})]", decl.name)?;
		writeln!(
			out,
			"\t\t{}({}),",
			decl.variant_name(&cls.name),
			decl.rust_name()
		)?;
	}
	writeln!(out, "\t}}")?;
	writeln!(out, "\timpl TdType for {} {{", cls.name)?;

	writeln!(
		out,
		"\t\tfn type_name(&self) -> &'static str {{ match self {{"
	)?;
	for decl in api.class_implementors(&cls.name) {
		writeln!(
			out,
			"\t\t\tSelf::{}(ref v) => v.type_name(),",
			decl.variant_name(&cls.name)
		)?;
	}
	writeln!(out, "\t\t}} }}")?;

	writeln!(out, "\t}}")?;
	Ok(())
}

fn additional_derives(name: &str) -> &'static str {
	match name {
		"ChatList" | "chatListMain" | "chatListArchive" | "chatListFilter" => ",PartialEq,Eq,Hash",
		_ => "",
	}
}

fn can_be_null(decl_name: &str, field: &Field) -> bool {
	match (decl_name, field.name.as_str()) {
		("messageSendOptions", "scheduling_state")
		| ("sendMessage", "reply_markup")
		| ("sendMessage", "message_thread_id") => true,
		_ => field.documentation.contains("null"),
	}
}
