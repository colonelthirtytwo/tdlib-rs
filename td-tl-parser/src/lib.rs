//! Parses just enough TL to parse the `td_api.tl` file and generate corresponding Rust structures.

#[macro_use]
extern crate pest_derive;
pub mod generate;
pub mod parsing;
