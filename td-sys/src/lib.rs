//! C bindings and Serde JSON types for tdlib

pub(crate) mod serde_util;
#[allow(non_camel_case_types)]
mod td_json_client;
#[allow(non_camel_case_types)]
mod td_log;

pub mod traits;

/// C bindings
pub mod c_bindings {
	pub use super::{
		td_json_client::*,
		td_log::*,
	};
}

/// Serde JSON types (auto-generated).
///
/// Split into three submodules:
///
/// * `objects`: Concrete structures of objects used in requests and responses
/// * `classes`: Enums corresponding to abstract base classes. Members are instances of the structures in `objects`
/// * `functions`: API function messages
pub mod json_types {
	include!(concat!(env!("OUT_DIR"), "/td_api.rs"));
}
