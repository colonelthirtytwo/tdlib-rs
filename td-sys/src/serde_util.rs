//! Serde-related helpers

use serde::{
	de,
	ser,
	Serialize,
};
use std::{
	convert::TryFrom,
	fmt,
	str::FromStr,
};

/// Serializes a byte array into a base64-encoded string.
///
/// For use with `#[serde(serialize_with="")]`
pub fn ser_base64<S: ser::Serializer>(v: &Vec<u8>, s: S) -> Result<S::Ok, S::Error> {
	base64::encode(v).serialize(s)
}

struct Base64Visitor;
impl<'de> de::Visitor<'de> for Base64Visitor {
	type Value = Vec<u8>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("a base64-encoded string")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		base64::decode(v).map_err(E::custom)
	}
}
/*struct OptBase64Visitor;
impl<'de> de::Visitor<'de> for OptBase64Visitor {
	type Value = Option<Vec<u8>>;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("a base64-encoded string")
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		base64::decode(v).map(|v| Some(v)).map_err(E::custom)
	}

	fn visit_some<D: de::Deserializer<'de>>(self, de: D) -> Result<Self::Value, D::Error> {
		de.deserialize_str(Base64Visitor).map(|v| Some(v))
	}

	fn visit_none<E: de::Error>(self) -> Result<Self::Value, E> {
		Ok(None)
	}
}*/
/// Deserializes a byte array from a base64-encoded string
///
/// For use with `#[serde(deserialize_with="")]`
pub fn de_base64<'de, D: de::Deserializer<'de>>(de: D) -> Result<Vec<u8>, D::Error> {
	de.deserialize_str(Base64Visitor)
}

/// Serializes an i64 in a json-safe way.
///
/// If the `i64` cannot be converted to an `f64` without losing information, it serializes
/// a string containing the i64 instead.
pub fn ser_large_int<S: ser::Serializer>(v: &i64, s: S) -> Result<S::Ok, S::Error> {
	if *v == ((*v as f64) as i64) {
		v.serialize(s)
	} else {
		v.to_string().serialize(s)
	}
}

struct IntOrStringVisitor;
impl<'de> de::Visitor<'de> for IntOrStringVisitor {
	type Value = i64;

	fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
		formatter.write_str("a 64-bit signed integer or string representation")
	}

	fn visit_i64<E: de::Error>(self, v: i64) -> Result<Self::Value, E> {
		Ok(v)
	}

	fn visit_u64<E: de::Error>(self, v: u64) -> Result<Self::Value, E> {
		i64::try_from(v).map_err(|_| E::invalid_value(de::Unexpected::Unsigned(v), &self))
	}

	fn visit_str<E: de::Error>(self, v: &str) -> Result<Self::Value, E> {
		i64::from_str(v).map_err(|_| E::invalid_value(de::Unexpected::Str(v), &self))
	}
}

/// Deserializes an i64 stored as an integer or string
///
/// Inverse of `ser_large_int`.
pub fn de_large_int<'de, D: de::Deserializer<'de>>(de: D) -> Result<i64, D::Error> {
	de.deserialize_any(IntOrStringVisitor)
}
