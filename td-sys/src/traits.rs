//! Traits that the auto-generate types use

use serde::{
	de::DeserializeOwned,
	Serialize,
};

/// Generic TDlib type
pub trait TdType {
	/// Gets the type name, as it appears in the `@type`
	fn type_name(&self) -> &'static str;
}

/// Class that a type implements (if any)
pub trait TdClassImpl: TdType {
	/// The class being implemented
	type Class: TdType;
	/// Convert the object to the class
	fn upcast(self) -> Self::Class;
	/// Checks if the class is an instance of this object, and gets it if it is
	fn downcast(sup: Self::Class) -> Result<Self, Self::Class>
	where
		Self: Sized;
}

/// API request object.
pub trait TdFunction: Serialize {
	/// The type that the API returns
	type Response: TdType + DeserializeOwned;
	/// Gets the type name, as it appears in the `@type`
	fn type_name(&self) -> &'static str;
}
