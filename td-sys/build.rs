fn generate_types() {
	use std::{
		env,
		fs,
		path::Path,
	};
	use td_tl_parser::generate::generate_and_write;

	println!("cargo:rerun-if-changed=../td/td/generate/scheme/td_api.tl");
	let src = fs::read_to_string("../td/td/generate/scheme/td_api.tl")
		.expect("Could not read td/generate/scheme/td_api.tl (did you clone recursively?)");
	let out = fs::File::create(Path::new(&env::var_os("OUT_DIR").unwrap()).join("td_api.rs"))
		.expect("Could not create out file");
	generate_and_write(&src, out).expect("Could not generate json types");
}

#[cfg(feature = "link-static")]
fn link() {
	for name in (&[
		"tdjson_static",
		"tdjson_private",
		"tdclient",
		"tdapi",
		"tdnet",
		"tddb",
		"tdactor",
		"tdsqlite",
		"tdcore",
		"tdutils",
	])
		.iter()
		.copied()
		.rev()
	{
		println!("cargo:rustc-link-lib=static={}", name);
	}
	println!("cargo:rustc-link-lib=stdc++");
	println!("cargo:rustc-link-lib=ssl");
	println!("cargo:rustc-link-lib=crypto");
	println!("cargo:rustc-link-lib=dl");
	println!("cargo:rustc-link-lib=z");
}

#[cfg(not(feature = "link-static"))]
fn link() {
	println!("cargo:rustc-link-lib=tdjson");
}

#[cfg(feature = "build-td")]
fn build() {
	use cmake::Config;
	let dst = Config::new("../td").build_target("tdjson_static").build();
	for name in &[
		"", "/tdnet", "/tdactor", "/sqlite", "/tddb", "/tdutils", "/tdtl",
	] {
		println!(
			"cargo:rustc-link-search=native={}/build{}",
			dst.display(),
			name
		);
	}
}

#[cfg(not(feature = "build-td"))]
fn build() {}

fn main() {
	generate_types();
	build();
	link();
}
