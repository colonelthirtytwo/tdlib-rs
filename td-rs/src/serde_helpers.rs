use serde::{
	Deserialize,
	Serialize,
};

/// Helper for deserializing the `@type` and `@extra` fields from a message.
///
/// You can then use this information to decide to route the request to
/// the response and/or deserialize it into a more specific type.
#[derive(Debug, Deserialize)]
pub struct DeserTypeAndExtra<'a, Extra> {
	#[serde(rename = "@type")]
	pub typ: &'a str,
	#[serde(rename = "@extra")]
	pub extra: Option<Extra>,
}

/// Helper for serializing an object with additional `@type` and `@extra` fields.
#[derive(Debug, Serialize)]
pub struct SerWithTypeAndExtra<T, Extra> {
	#[serde(rename = "@type")]
	pub typ: &'static str,
	#[serde(flatten)]
	pub obj: T,
	#[serde(rename = "@extra")]
	pub extra: Extra,
}
impl<T, Extra> SerWithTypeAndExtra<T, Extra>
where
	T: td_sys::traits::TdFunction,
{
	pub fn new(obj: T, extra: Extra) -> Self {
		Self {
			typ: obj.type_name(),
			obj,
			extra,
		}
	}
}
