use std::{
	ffi::CStr,
	os::raw::c_char,
	sync::Once,
};
use td_sys::{
	c_bindings,
	traits::TdClassImpl,
};

use crate::{
	json_types::{
		functions,
		objects,
	},
	raw_client::TdRawClient,
	serde_helpers::SerWithTypeAndExtra,
};

static GLOBAL_SETUP: Once = Once::new();

/// Does global setup, including:
///
/// * Sets the tdlib fatal error callback to a function that prints the error to stderr,
/// * Sets the tdlib message callback to a function that forwards messages to the `log` crate under the name "tdlib",
/// * Sets the log stream to use the previous callback, and
/// * Sets the log level to match the configured log level for `"tdlib"`.
///
/// Creating a `TdJsonClient`, directly or indirectly, will do this setup, but you can do it manually to.
///
/// Setup is only done once (protected by `std::sync::Once`).
pub fn global_setup() {
	GLOBAL_SETUP.call_once(|| {
		unsafe {
			c_bindings::td_set_log_fatal_error_callback(Some(fatal_error_cb));
			c_bindings::td_set_log_message_callback(Some(log_cb));
		}

		TdRawClient::execute_ser(
			&SerWithTypeAndExtra::new(
				functions::SetLogStream {
					log_stream: objects::LogStreamCallback {}.upcast(),
				},
				(),
			),
			|_| (),
		)
		.unwrap();

		let tdlib_level = if log::log_enabled!(target: "tdlib", log::Level::Trace) {
			1023
		} else if log::log_enabled!(target: "tdlib", log::Level::Debug) {
			4
		} else if log::log_enabled!(target: "tdlib", log::Level::Info) {
			3
		} else if log::log_enabled!(target: "tdlib", log::Level::Warn) {
			2
		} else if log::log_enabled!(target: "tdlib", log::Level::Error) {
			1
		} else {
			0
		};

		TdRawClient::execute_ser(
			&SerWithTypeAndExtra::new(
				functions::SetLogVerbosityLevel {
					new_verbosity_level: tdlib_level,
				},
				(),
			),
			|_| (),
		)
		.unwrap();
	});
}

unsafe extern "C" fn fatal_error_cb(msg: *const c_char) {
	let s = CStr::from_ptr(msg).to_string_lossy();
	eprintln!("tdlib fatal error: {}", s);
}
unsafe extern "C" fn log_cb(level: i32, msg: *const c_char) {
	let s = CStr::from_ptr(msg).to_string_lossy();
	let rust_level = match level {
		2 => log::Level::Warn,
		3 => log::Level::Info,
		4 => log::Level::Debug,
		v if v <= 1 => log::Level::Error,
		_ => log::Level::Trace,
	};
	log::log!(target: "tdlib", rust_level, "{}", s.trim());
}
