//! Raw API client that works with JSON strings

use serde::Serialize;
use std::{
	borrow::Cow,
	cell::Cell,
	ffi::{
		c_void,
		CStr,
		CString,
	},
	marker::PhantomData,
	os::raw::c_char,
	ptr::{
		null_mut,
		NonNull,
	},
	str::from_utf8,
	sync::atomic::{
		AtomicBool,
		Ordering,
	},
};
use td_sys::c_bindings as sys;

std::thread_local! {
	/// Set if we are using the string in libtd's buffer, so that receive and execute calls should fail
	static IS_IN_RECV_CB: Cell<bool> = Cell::new(false);
}

/// Safe access to the JSON client.
///
/// Exposes an API for passing the client strings of JSON. As such, it's a low-level "raw"
/// interface.
pub struct TdRawClient {
	p: NonNull<c_void>,
	has_recv: AtomicBool,
}
impl TdRawClient {
	pub fn new() -> Self {
		crate::global_setup();

		// TODO: what is td's failure modes?
		let p = unsafe { sys::td_json_client_create() };
		let p = NonNull::new(p).expect("td_json_client_create returned null");
		Self {
			p,
			has_recv: AtomicBool::new(true),
		}
	}

	/// Sends a message, which is a string (`str` or `CStr`).
	///
	/// Passing in a `str` will require copying its contents to make it C safe; passing in
	/// other `IntoCStr` types like `CStr` does not.
	pub fn send_raw<'a, S: IntoCStr<'a>>(&self, msg: S) {
		let cstr = msg.into_cstr();
		log::trace!("TdRawClient Send: {:?}", cstr);
		unsafe { sys::td_json_client_send(self.p.as_ptr(), cstr.as_ptr()) };
	}

	/// Sends a message, which is a serde_json compatible object
	///
	/// Will allocate a buffer to hold the serialized data; for repeated sends, you may want to
	/// have your own buffer and send it manuall with `send`.
	pub fn send_ser<S: Serialize>(&self, obj: &S) -> Result<(), serde_json::Error> {
		let cstr = CString::new(serde_json::to_vec(obj)?).unwrap();
		self.send_raw(cstr);
		Ok(())
	}

	/// Gets the receiver, if none exists already.
	///
	/// While multiple threads can send messages with the same `TdRawClient` object, only one
	/// thread may receive messages at a time.
	///
	/// To enforce this, only one `TdRawClientReceiver` can exist in any point in time for a
	/// particular client; calls after the first `receiver` but before the receiver is dropped
	/// will return `None`.
	pub fn receiver<'a>(&'a self) -> Option<TdRawClientReceiver<'a>> {
		if self.has_recv.fetch_and(false, Ordering::Acquire) {
			Some(TdRawClientReceiver {
				client: self,
				_dont_impl_sync: Default::default(),
			})
		} else {
			None
		}
	}

	/// Sends a synchronous message, which is a string (`str` or `CStr`), and gets the response.
	///
	/// Calls the provided callback with the response string. Returns the result of the callback function.
	/// But see `TdRawClientReceiver.receive` for caviats!
	///
	/// Only certain messages can be sent this way; consult the tdlib documentation.
	///
	/// Passing in a `str` will require copying its contents to make it C safe; passing in
	/// other `IntoCStr` types like `CStr` does not.
	pub fn execute_raw<'a, S: IntoCStr<'a>, R, Cb: FnOnce(&str) -> R>(msg: S, cb: Cb) -> R {
		let cstr = msg.into_cstr();

		let _lock = InRecvCbLock::acquire();
		let res = unsafe { cchar_to_str(sys::td_json_client_execute(null_mut(), cstr.as_ptr())) };
		cb(res.unwrap_or(""))
	}

	/// Sends a message, which is a serde_json compatible object
	///
	/// A buffer will be allocated to store the serialied result. For repated sends,
	/// you may want to reuse a buffer with `execute_raw`.
	pub fn execute_ser<S: Serialize, R, Cb: FnOnce(&str) -> R>(
		msg: &S,
		cb: Cb,
	) -> Result<R, serde_json::Error> {
		let cstr = CString::new(serde_json::to_vec(msg)?).unwrap();
		Ok(Self::execute_raw(&cstr, cb))
	}
}
impl Drop for TdRawClient {
	fn drop(&mut self) {
		unsafe { sys::td_json_client_destroy(self.p.as_ptr()) };
	}
}
unsafe impl Send for TdRawClient {}
unsafe impl Sync for TdRawClient {}

/// Object representing the privilege to receive messages.
///
/// While multiple threads can send messages with the same `TdRawClient` object, only one
/// thread may receive messages. This receiver represents that privilege.
///
/// Dropping this object will release that privilege, allowing `TdRawClient.receiver()` to return
/// another object.
pub struct TdRawClientReceiver<'a> {
	client: &'a TdRawClient,
	_dont_impl_sync: PhantomData<*const c_void>,
}
impl<'a> TdRawClientReceiver<'a> {
	/// Receives a message.
	///
	/// Timeout is in seconds. Calls the provided callback with the response string if a response was received,
	/// or `None` if the timeout was reached. Returns the result of the callback function.
	///
	/// Callback Caviats
	/// ----------------
	///
	/// The callback is passed a direct reference to libtd's thread-local response string buffer. This buffer is only
	/// valid until the next receiving call in the thread, thus __calling `TdRawClientReceiver.receive`, `TdRawClient.execute_raw`,
	/// or any of their variants in the callback function will cause a panic.__
	///
	/// Use the callback's return to export owned data to the caller. An extremely basic callback would be `|r| r.map(String::new)`,
	/// which would copy the message to an owned buffer that is safe to return. However for best performance, you will likely want
	/// to run parsing on the borrowed string to avoid copying it.
	pub fn receive<R, Cb: FnOnce(Option<&str>) -> R>(&self, timeout: f64, cb: Cb) -> R {
		let _lock = InRecvCbLock::acquire();

		let res =
			unsafe { cchar_to_str(sys::td_json_client_receive(self.client.p.as_ptr(), timeout)) };
		log::trace!("TdRawClient Recv: {:?}", res);
		cb(res)
	}
}
unsafe impl<'a> Send for TdRawClientReceiver<'a> {}
impl<'a> Drop for TdRawClientReceiver<'a> {
	fn drop(&mut self) {
		let v = self.client.has_recv.fetch_or(true, Ordering::Release);
		assert!(!v, "Somehow dropping a TdRawClientReceiver but the TdRawClient thinks that no one has it. This is a bug.");
	}
}

/// Trait for types that can be converted to a C string.
///
/// Implemented for `str`, `String`, `CStr`, and `CString`. Of these, only
/// `str` requires a copy; the other formats either use an existing buffer or
/// are already in the needed format.
pub trait IntoCStr<'a> {
	/// Converts self into a C string, which can be borrowed or owned.
	///
	/// Panics
	/// ------
	///
	/// If the source type has a string that contains null bytes.
	fn into_cstr(self) -> Cow<'a, CStr>;
}
impl<'a> IntoCStr<'a> for &'a str {
	fn into_cstr(self) -> Cow<'a, CStr> {
		CString::new(String::from(self)).unwrap().into()
	}
}
impl<'a> IntoCStr<'a> for &'a String {
	fn into_cstr(self) -> Cow<'a, CStr> {
		CString::new(String::from(self)).unwrap().into()
	}
}
impl<'a> IntoCStr<'a> for String {
	fn into_cstr(self) -> Cow<'a, CStr> {
		CString::new(self).unwrap().into()
	}
}
impl<'a> IntoCStr<'a> for &'a CStr {
	fn into_cstr(self) -> Cow<'a, CStr> {
		self.into()
	}
}
impl<'a> IntoCStr<'a> for CString {
	fn into_cstr(self) -> Cow<'a, CStr> {
		self.into()
	}
}
impl<'a> IntoCStr<'a> for &'a CString {
	fn into_cstr(self) -> Cow<'a, CStr> {
		self.into()
	}
}

unsafe fn cchar_to_str<'a>(p: *const c_char) -> Option<&'a str> {
	if p.is_null() {
		return None;
	}
	Some(from_utf8(CStr::from_ptr(p).to_bytes()).expect("tdlib did not return valid utf-8 text"))
}

/// Per-thread lock guard for `execute_raw`/`receive`.
struct InRecvCbLock;
impl InRecvCbLock {
	pub fn acquire() -> Self {
		IS_IN_RECV_CB.with(|flag| {
			if flag.get() {
				panic!(
					"Re-enterant call to `TdRawClient.execute_raw` or `TdRawClientReceiver.receive`."
				);
			}
			flag.set(true);
		});
		Self
	}
}
impl Drop for InRecvCbLock {
	fn drop(&mut self) {
		IS_IN_RECV_CB.with(|flag| {
			let f = flag.replace(false);
			assert!(
				f,
				"Dropped InRecvCbLock without the flag being set; this is a bug"
			);
		})
	}
}
