use futures_util::{
	future::join_all,
	stream::{
		self,
		Stream,
		TryStreamExt,
	},
};
use td_sys::json_types::{
	classes,
	functions,
	objects,
};

use crate::tokio_client::{
	Error,
	TD,
};

impl TD {
	/// Gets all of the chats in a certain list (active, archived, etc).
	pub fn chats<'a>(
		&'a self,
		filter: classes::ChatList,
	) -> impl Stream<Item = Result<objects::Chat, Error>> + 'a {
		stream::unfold(i64::max_value(), move |last_id| {
			let filter = filter.clone();
			async move {
				let req = functions::GetChats {
					chat_list: filter.clone(),
					offset_order: last_id,
					offset_chat_id: 0,
					limit: 100,
				};
				let resp: Result<objects::Chats, Error> = self.send(&req).await;
				let ids = match resp {
					Ok(objects::Chats { ref chat_ids, .. }) if chat_ids.is_empty() => {
						return None;
					}
					Ok(objects::Chats { chat_ids, .. }) => chat_ids,
					Err(e) => {
						return Some((Err(e), i64::min_value()));
					}
				};
				let chats = join_all(ids.into_iter().map(|id| self.chat(id))).await;
				let next_order = chats
					.iter()
					.filter_map(|ch| ch.as_ref().ok())
					.map(|ch| {
						ch.positions
							.iter()
							.find(|pos| pos.list == filter)
							.map(|pos| pos.order)
							.unwrap_or(i64::max_value())
					})
					.min()
					.unwrap_or(i64::min_value());
				return Some((Ok(stream::iter(chats)), next_order));
			}
		})
		.try_flatten()
	}

	/// Gets info about a chat by ID
	pub async fn chat(&self, id: i64) -> Result<objects::Chat, Error> {
		self.send(&functions::GetChat { chat_id: id }).await
	}

	pub async fn send_message(
		&self,
		chat_id: i64,
		reply_to_msg: Option<i64>,
		options: Option<objects::MessageSendOptions>,
		content: classes::InputMessageContent,
	) -> Result<objects::Message, Error> {
		self.send(&functions::SendMessage {
			chat_id,
			reply_to_message_id: reply_to_msg.unwrap_or(0),
			options: options.unwrap_or(objects::MessageSendOptions {
				disable_notification: false,
				from_background: false,
				scheduling_state: None,
			}),
			reply_markup: None,
			message_thread_id: None,
			input_message_content: content,
		})
		.await
	}
}
