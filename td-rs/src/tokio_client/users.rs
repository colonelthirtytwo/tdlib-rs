use td_sys::json_types::{
	functions,
	objects,
};

use crate::tokio_client::{
	Error,
	TD,
};

impl TD {
	pub async fn user(&self, id: i32) -> Result<objects::User, Error> {
		self.send(&functions::GetUser { user_id: id }).await
	}

	pub async fn user_full(&self, id: i32) -> Result<objects::UserFullInfo, Error> {
		self.send(&functions::GetUserFullInfo { user_id: id }).await
	}
}
