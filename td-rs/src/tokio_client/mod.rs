mod auth;
mod chats;
mod users;

use slab::Slab;
use std::{
	fmt,
	sync::{
		atomic::{
			AtomicBool,
			Ordering,
		},
		Arc,
		Mutex,
	},
};
use td_sys::traits::*;
use tokio::sync::{
	mpsc,
	oneshot,
};

use crate::{
	json_types::{
		classes::Update,
		functions,
		objects,
	},
	raw_client::TdRawClient,
	serde_helpers::{
		DeserTypeAndExtra,
		SerWithTypeAndExtra,
	},
};

pub struct TD {
	shared: Arc<ReceiverShared>,
	/// Only an option so that stop can take and await it
	recv_task: Option<tokio::task::JoinHandle<()>>,
}
impl TD {
	pub fn new() -> (Self, mpsc::UnboundedReceiver<Update>) {
		let shared = Arc::new(ReceiverShared {
			raw_client: TdRawClient::new(),
			run_flag: AtomicBool::new(true),
			pending_requests: Mutex::new(Slab::new()),
		});

		let (event_sender, event_receiver) = mpsc::unbounded_channel();
		let recv_task = Self::recv_task(event_sender, Arc::clone(&shared));

		let this = Self {
			shared,
			recv_task: Some(recv_task),
		};

		(this, event_receiver)
	}

	fn recv_task(
		event_sender: mpsc::UnboundedSender<Update>,
		shared: Arc<ReceiverShared>,
	) -> tokio::task::JoinHandle<()> {
		tokio::task::spawn_blocking(move || {
			log::info!("Receiver starting");
			let receiver = shared.raw_client.receiver().unwrap();
			while shared.run_flag.load(Ordering::Acquire) {
				let _: Option<()> = receiver.receive(999999.9, |text| {
					log::trace!("Receiver message: {:?}", text);
					let text = text?;
					let DeserTypeAndExtra { typ, extra } = serde_json::from_str(text)
						.map_err(|e| {
							error!("Could not deserialize message from td, dropping. Error: {} / Message: {:?}", e, text)
						})
						.ok()?;

					if let Some(handle) = extra {
						let func = {
							let mut slab = shared.pending_requests.lock().unwrap();
							slab.remove(handle)
						};
						func(typ, text);
					} else if typ == "error" {
						let err = serde_json::from_str::<objects::Error>(text)
							.map_err(|_| {
								error!("Could not deserialize error from td. Message: {:?}", text)
							})
							.ok()?;
						error!("td returned error that did not correspond to a request. Error is: {:?}", err);
					} else {
						let update = serde_json::from_str::<Update>(text)
							.map_err(|e| {
								error!("Could not deserialize update from td, dropping. Error: {} / Message: {:?}", e, text)
							})
							.ok()?;
						let _ = event_sender.send(update);
					}
					Some(())
				});
			}
			log::info!("Receiver exiting");
		})
	}

	/// Gracefully stops the server, waiting for the receiver thread to shut down.
	pub async fn stop(mut self) {
		self.stop_receiver().await;
		self.recv_task.take().unwrap().await.unwrap();
	}

	pub async fn stop_receiver(&self) {
		self.shared.run_flag.store(false, Ordering::Release);
		self.send(&functions::SetAlarm { seconds: 0. })
			.await
			.unwrap();
	}

	pub async fn send<T: TdFunction + Send + 'static>(&self, msg: &T) -> Result<T::Response, Error>
	where
		T::Response: Send,
	{
		// Send errors are ignored here; can't really do much about it
		let (sender, receiver) = oneshot::channel();
		let cb = Box::new(move |typ: &str, msg: &str| {
			if let Err(err) = Error::check(typ, msg) {
				let _ = sender.send(Err(err));
				return;
			}
			let _ = sender.send(serde_json::from_str::<T::Response>(msg).map_err(Error::from));
		});

		let handle = {
			let mut slab = self.shared.pending_requests.lock().unwrap();
			slab.insert(cb)
		};

		self.shared.raw_client.send_ser(&SerWithTypeAndExtra {
			typ: msg.type_name(),
			obj: msg,
			extra: handle,
		})?;

		match receiver.await {
			Ok(Ok(v)) => Ok(v),
			Ok(Err(v)) => Err(v),
			Err(v) => Err(v.into()),
		}
	}

	/// Sets the library parameters.
	///
	/// Wrapper for the `setTdlibParameters` API function.
	pub async fn lib_parameters(&self, params: objects::TdlibParameters) -> Result<(), Error> {
		self.send(&functions::SetTdlibParameters { parameters: params })
			.await
			.map(|objects::Ok {}| {})
	}
}
impl Drop for TD {
	fn drop(&mut self) {
		self.shared.run_flag.store(false, Ordering::Release);
		// todo: alert task to stop. tdlib has a message for this.
	}
}

/// Shared between the TD client and the receiver task.
struct ReceiverShared {
	/// Raw JSON client
	raw_client: TdRawClient,
	/// true if the receiver should still be running, false to tell it to exit
	run_flag: AtomicBool,
	/// Outstanding requests, waiting for results from the library.
	///
	/// IDs from the slab are put in the @extra field of the request, so that we can look
	/// up the matching handler when we receive the response.
	///
	/// TODO: replace with lock-free slab
	pending_requests: Mutex<Slab<Box<dyn FnOnce(&str, &str) + Send + 'static>>>,
}

/// Error from the API
#[derive(Debug)]
pub enum Error {
	/// tdlib reported an error.
	TDError { code: i32, message: String },
	/// The receiver task was terminated before a response was received.
	Closed(oneshot::error::RecvError),
	/// Could not deserialize the tdlib response.
	SerdeError(serde_json::Error),
	/// Miscellaneous error
	Misc(String),
}
impl Error {
	pub(crate) fn check(typ: &str, msg: &str) -> Result<(), Self> {
		if typ == "error" {
			serde_json::from_str::<objects::Error>(msg)
				.map_err(Error::from)
				.and_then(|e| Err(Error::from(e)))
		} else {
			Ok(())
		}
	}
}
impl fmt::Display for Error {
	fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
		match self {
			Self::TDError { code, message } => write!(f, "({}) {}", code, message),
			Self::Closed(e) => e.fmt(f),
			Self::SerdeError(e) => e.fmt(f),
			Self::Misc(e) => e.fmt(f),
		}
	}
}
impl std::error::Error for Error {
	fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
		match self {
			Self::Closed(ref e) => Some(e),
			Self::SerdeError(ref e) => Some(e),
			_ => None,
		}
	}
}
impl<'a> From<objects::Error> for Error {
	fn from(v: objects::Error) -> Self {
		Self::TDError {
			code: v.code,
			message: v.message,
		}
	}
}
impl From<oneshot::error::RecvError> for Error {
	fn from(v: oneshot::error::RecvError) -> Self {
		Self::Closed(v)
	}
}
impl From<serde_json::Error> for Error {
	fn from(v: serde_json::Error) -> Self {
		Self::SerdeError(v)
	}
}
