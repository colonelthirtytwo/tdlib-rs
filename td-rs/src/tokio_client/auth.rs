use crate::tokio_client::{
	Error,
	TD,
};
use td_sys::json_types::{
	functions,
	objects,
};

impl TD {
	/// Sets the local database encryption key.
	///
	/// Wraps `SetDatabaseEncryptionKey`.
	pub async fn db_encryption_key(&self, key: &[u8]) -> Result<(), Error> {
		self.send(&functions::SetDatabaseEncryptionKey {
			new_encryption_key: Vec::from(key),
		})
		.await
		.map(|objects::Ok {}| {})
	}

	pub async fn phone_number(
		&self,
		num: &str,
		settings: objects::PhoneNumberAuthenticationSettings,
	) -> Result<(), Error> {
		self.send(&functions::SetAuthenticationPhoneNumber {
			phone_number: num.into(),
			settings,
		})
		.await
		.map(|objects::Ok {}| {})
	}

	pub async fn auth_code(&self, code: &str) -> Result<(), Error> {
		self.send(&functions::CheckAuthenticationCode { code: code.into() })
			.await
			.map(|objects::Ok {}| {})
	}

	pub async fn accept_tos(&self, tos_id: Option<&str>) -> Result<(), Error> {
		self.send(&functions::AcceptTermsOfService {
			terms_of_service_id: tos_id.map(Into::into).unwrap_or(String::new()),
		})
		.await
		.map(|objects::Ok {}| {})
	}
}
