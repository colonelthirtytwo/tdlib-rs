#[macro_use]
extern crate log;

use futures_util::{
	future::FutureExt,
	pin_mut,
	select,
	stream::StreamExt,
};
use std::sync::Arc;
use structopt::StructOpt;
use td::{
	json_types::{
		classes::*,
		objects::*,
	},
	tokio_client::TD,
};
use tokio::sync::{
	mpsc,
	oneshot,
};

#[derive(StructOpt, Debug)]
struct Args {
	pub api_key: i32,
	pub api_hash: String,
}

#[tokio::main]
async fn main() {
	flexi_logger::Logger::with_env().start().unwrap();

	let args = Args::from_args();

	let rl = rustyline::Editor::<()>::new();

	let (td, td_updates) = TD::new();
	let td = Arc::new(td);

	let (prompt_sender, prompt_receiver) = mpsc::unbounded_channel();

	let recv_task = tokio::task::spawn(recv_task(
		args.api_key,
		args.api_hash,
		Arc::clone(&td),
		td_updates,
		prompt_sender,
	))
	.fuse();

	let prompt_task = tokio::task::spawn(prompt_task(Arc::clone(&td), rl, prompt_receiver)).fuse();

	pin_mut!(recv_task);
	pin_mut!(prompt_task);

	// declare these as closures since select! recurses a lot
	let check_recv_task_exit = |res| match res {
		Ok(()) => unreachable!(),
		Err(e) => {
			error!("Receiver thread exited unexpectedly, exiting: {}", e);
			std::process::exit(1);
		}
	};
	let check_prompt_task_exit = |res| match res {
		Ok(Ok(())) => {
			unreachable!();
		}
		Ok(Err(rustyline::error::ReadlineError::Eof))
		| Ok(Err(rustyline::error::ReadlineError::Interrupted)) => {}
		Ok(Err(e)) => {
			error!("Prompt error, exiting: {}", e);
			std::process::exit(1);
		}
		Err(e) => {
			error!("Prompt task exited unexpectedly, exiting: {}", e);
			std::process::exit(1);
		}
	};

	select! {
		res = recv_task => check_recv_task_exit(res),
		res = prompt_task => check_prompt_task_exit(res),
	};

	td.stop_receiver().await;
}

async fn recv_task(
	api_key: i32,
	api_hash: String,
	td: Arc<TD>,
	mut td_updates: mpsc::UnboundedReceiver<Update>,
	prompt_events: mpsc::UnboundedSender<PromptTaskMsg>,
) {
	while let Some(ev) = td_updates.recv().await {
		debug!("Update: {:?}", ev);
		match ev {
			Update::AuthorizationState(UpdateAuthorizationState {
				authorization_state: AuthorizationState::WaitTdlibParameters(_),
			}) => {
				let td = Arc::clone(&td);
				let api_hash = api_hash.clone();
				tokio::task::spawn(async move {
					td.lib_parameters(TdlibParameters {
						use_test_dc: false,
						database_directory: "./tdlib-data".into(),
						files_directory: "./tdlib-files".into(),
						use_file_database: true,
						use_chat_info_database: true,
						use_message_database: true,
						use_secret_chats: true,
						api_id: api_key,
						api_hash: api_hash,
						system_language_code: "en".into(),
						device_model: "Desktop".into(),
						application_version: "0.1.0".into(),
						system_version: "0.1.0".into(),
						enable_storage_optimizer: true,
						ignore_file_names: false,
					})
					.await
					.unwrap();
				});
			}
			Update::AuthorizationState(UpdateAuthorizationState {
				authorization_state: AuthorizationState::WaitEncryptionKey(_),
			}) => {
				let td = Arc::clone(&td);
				tokio::task::spawn(async move {
					td.db_encryption_key(&[]).await.unwrap();
				});
			}
			Update::AuthorizationState(UpdateAuthorizationState {
				authorization_state: AuthorizationState::WaitPhoneNumber(_),
			}) => {
				let td = Arc::clone(&td);
				let mut prompt_events = prompt_events.clone();
				tokio::task::spawn(async move {
					let phone = prompt(&mut prompt_events, "Enter phone number: ").await;
					td.phone_number(
						&phone,
						PhoneNumberAuthenticationSettings {
							allow_flash_call: true,
							is_current_phone_number: false,
							allow_sms_retriever_api: false,
						},
					)
					.await
					.unwrap();
				});
			}
			Update::AuthorizationState(UpdateAuthorizationState {
				authorization_state: AuthorizationState::WaitCode(_),
			}) => {
				let td = Arc::clone(&td);
				let mut prompt_events = prompt_events.clone();
				tokio::task::spawn(async move {
					let code = prompt(&mut prompt_events, "Enter authorization code: ").await;
					td.auth_code(&code).await.unwrap();
				});
			}
			Update::AuthorizationState(UpdateAuthorizationState {
				authorization_state: AuthorizationState::WaitRegistration(reg),
			}) => {
				let td = Arc::clone(&td);
				let mut prompt_events = prompt_events.clone();
				tokio::task::spawn(async move {
					let tos = reg.terms_of_service;
					if tos.show_popup {
						let tos_text = tos.text.text;
						let text = prompt(
							&mut prompt_events,
							&format!("{}\nDo you accept the Telegram ToS?: ", tos_text),
						)
						.await;
						match text.as_str() {
							"y" | "Y" | "yes" | "Yes" | "YES" => {
								td.accept_tos(None).await.unwrap();
							}
							_ => {
								// TODO: do this in a better way
								std::process::exit(0);
							}
						}
					} else {
						info!("Accepting TOS automatically");
						td.accept_tos(None).await.unwrap();
					}
				});
			}
			Update::AuthorizationState(UpdateAuthorizationState {
				authorization_state: AuthorizationState::Ready(_),
			}) => {
				info!("Auth OK, ready to go");
				prompt_events
					.send(PromptTaskMsg::StartCommands)
					.map_err(|e| e.to_string())
					.unwrap();
			}
			Update::NewMessage(UpdateNewMessage { message }) => {
				let td = Arc::clone(&td);
				tokio::task::spawn(async move {
					let sender = if message.sender_user_id != 0 {
						Some(td.user(message.sender_user_id).await.unwrap())
					} else {
						None
					};

					let chat = td.chat(message.chat_id).await.unwrap();

					println!(
						"\r{:20} | {:20} | {}",
						chat.title,
						sender
							.as_ref()
							.map(|u| u.username.as_str())
							.unwrap_or("(None)"),
						match message.content {
							MessageContent::MessageText(ref text) => text.text.text.as_str(),
							MessageContent::MessageAnimation(_) => "(Animation)",
							MessageContent::MessageAudio(_) => "(Audio)",
							MessageContent::MessageDocument(_) => "(Document)",
							MessageContent::MessagePhoto(_) => "(Photo)",
							MessageContent::MessageExpiredPhoto(_) => "(ExpiredPhoto)",
							MessageContent::MessageSticker(_) => "(Sticker)",
							MessageContent::MessageVideo(_) => "(Video)",
							MessageContent::MessageExpiredVideo(_) => "(ExpiredVideo)",
							MessageContent::MessageVideoNote(_) => "(VideoNote)",
							MessageContent::MessageVoiceNote(_) => "(VoiceNote)",
							MessageContent::MessageLocation(_) => "(Location)",
							MessageContent::MessageVenue(_) => "(Venue)",
							MessageContent::MessageContact(_) => "(Contact)",
							MessageContent::MessageDice(_) => "(Dice)",
							MessageContent::MessageGame(_) => "(Game)",
							MessageContent::MessagePoll(_) => "(Poll)",
							MessageContent::MessageInvoice(_) => "(Invoice)",
							MessageContent::MessageCall(_) => "(Call)",
							MessageContent::MessageBasicGroupChatCreate(_) =>
								"(BasicGroupChatCreate)",
							MessageContent::MessageSupergroupChatCreate(_) =>
								"(SupergroupChatCreate)",
							MessageContent::MessageChatChangeTitle(_) => "(ChatChangeTitle)",
							MessageContent::MessageChatChangePhoto(_) => "(ChatChangePhoto)",
							MessageContent::MessageChatDeletePhoto(_) => "(ChatDeletePhoto)",
							MessageContent::MessageChatAddMembers(_) => "(ChatAddMembers)",
							MessageContent::MessageChatJoinByLink(_) => "(ChatJoinByLink)",
							MessageContent::MessageChatDeleteMember(_) => "(ChatDeleteMember)",
							MessageContent::MessageChatUpgradeTo(_) => "(ChatUpgradeTo)",
							MessageContent::MessageChatUpgradeFrom(_) => "(ChatUpgradeFrom)",
							MessageContent::MessagePinMessage(_) => "(PinMessage)",
							MessageContent::MessageScreenshotTaken(_) => "(ScreenshotTaken)",
							MessageContent::MessageChatSetTtl(_) => "(ChatSetTtl)",
							MessageContent::MessageCustomServiceAction(_) =>
								"(CustomServiceAction)",
							MessageContent::MessageGameScore(_) => "(GameScore)",
							MessageContent::MessagePaymentSuccessful(_) => "(PaymentSuccessful)",
							MessageContent::MessagePaymentSuccessfulBot(_) =>
								"(PaymentSuccessfulBot)",
							MessageContent::MessageContactRegistered(_) => "(ContactRegistered)",
							MessageContent::MessageWebsiteConnected(_) => "(WebsiteConnected)",
							MessageContent::MessagePassportDataSent(_) => "(PassportDataSent)",
							MessageContent::MessagePassportDataReceived(_) =>
								"(PassportDataReceived)",
							MessageContent::MessageUnsupported(_) => "(Unsupported)",
						}
					);
				});
			}
			_ => {}
		}
	}
}

enum PromptTaskMsg {
	Prompt(String, oneshot::Sender<String>),
	StartCommands,
}

async fn rl_do(
	rl: &mut Option<rustyline::Editor<()>>,
	prompt: String,
) -> Result<String, rustyline::error::ReadlineError> {
	let mut rl_obj = rl.take().unwrap();
	let (rl_obj, result) = tokio::task::spawn_blocking(move || {
		let result = rl_obj.readline(&prompt);
		if let Ok(ref s) = result {
			rl_obj.add_history_entry(s);
		}
		(rl_obj, result)
	})
	.await
	.unwrap();
	*rl = Some(rl_obj);
	result
}

async fn prompt(sender: &mut mpsc::UnboundedSender<PromptTaskMsg>, p: &str) -> String {
	let (res_send, res_recv) = oneshot::channel();
	sender
		.send(PromptTaskMsg::Prompt(p.into(), res_send))
		.map_err(|e| e.to_string())
		.unwrap();
	res_recv.await.unwrap()
}
async fn prompt_task(
	td: Arc<TD>,
	rl: rustyline::Editor<()>,
	mut events: mpsc::UnboundedReceiver<PromptTaskMsg>,
) -> Result<(), rustyline::error::ReadlineError> {
	let mut rl = Some(rl);
	// Initial mode. Setup has to ask us a few things like our phone number.
	loop {
		match events.recv().await.unwrap() {
			PromptTaskMsg::Prompt(prompt, response) => {
				let _ = response.send(rl_do(&mut rl, prompt.into()).await?);
			}
			PromptTaskMsg::StartCommands => {
				break;
			}
		}
	}
	std::mem::drop(events);

	loop {
		let s = rl_do(&mut rl, "telegram>".into()).await?;
		let args = match shell_words::split(&s) {
			Ok(v) => v,
			Err(e) => {
				println!("{}", e);
				continue;
			}
		};
		let cmd = match Command::from_iter_safe(args) {
			Ok(v) => v,
			Err(e) => {
				println!("{}", e);
				continue;
			}
		};

		match cmd {
			Command::Chats => {
				let chats_stream = td.chats(ChatList::Main(ChatListMain {}));
				pin_mut!(chats_stream);
				while let Some(chat_res) = chats_stream.next().await {
					let chat_info = match chat_res {
						Ok(v) => v,
						Err(e) => {
							error!("Error fetching chats: {}", e);
							continue;
						}
					};
					println!("{:20} {}", chat_info.id, chat_info.title);
				}
			}
			Command::Msg { id, text } => {
				match td
					.send_message(
						id,
						None,
						None,
						InputMessageContent::InputMessageText(InputMessageText {
							text: FormattedText {
								text,
								entities: vec![],
							},
							disable_web_page_preview: false,
							clear_draft: false,
						}),
					)
					.await
				{
					Ok(msg) => {
						println!("Sent. Message id: {}", msg.id);
					}
					Err(e) => {
						error!("Could not send message: {}", e);
					}
				}
			}
		}
	}
}

#[derive(Debug, StructOpt)]
#[structopt(
	no_version,
	global_settings=&[structopt::clap::AppSettings::NoBinaryName]
)]
enum Command {
	/// List chats
	Chats,
	/// Send a message to a channel
	Msg {
		/// Channel ID to send to
		id: i64,
		/// Message text
		text: String,
	},
}
